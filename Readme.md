# HC小区管理系统springboot版是前后端完全开源并免费商用的小区物业管理系统，商用建议使用分布式版本

## 开源代码说明

### HC小区前段代码
[https://gitee.com/java110/MicroCommunityWeb](https://gitee.com/java110/MicroCommunityWeb)

### HC分布式版本
[https://gitee.com/wuxw7/MicroCommunity](https://gitee.com/wuxw7/MicroCommunity)


### 如何开始（how to start）

[http://www.homecommunity.cn/](http://www.homecommunity.cn/)

### 如何安装（how to install）

centos7.6一键式自动安装

> yum install -y wget && wget https://dl.winqi.cn/hc/hcBootInstall.sh && sh hcBootInstall.sh 您的私网IP


### 硬件要求

2核cpu 4G内存 80G磁盘 （经测试内存空闲1.3G左右）

### 操作文档

[操作文档](http://www.homecommunity.cn/operateDoc)

### 技术文档

[技术文档](http://www.homecommunity.cn/document)

    
### 产品

   ![image](https://gitee.com/wuxw7/MicroCommunity/raw/master/docs/img/hc.png)

    
### 系统功能（function） 

   ![image](https://gitee.com/wuxw7/MicroCommunity/raw/master/docs/img/func.png)
   
   主要系统功能请查看
     
   [http://www.homecommunity.cn//document/#/func/funcation](http://www.homecommunity.cn//document/#/func/funcation)
  

### 演示地址（demo）

[http://www.homecommunity.cn/](http://www.homecommunity.cn/)

物业 账号/密码：wuxw/admin

代理商 账号/密码：dails/admin

运维团队 账号/密码：admin/admin

开发团队 账号/密码：dev/(由于开发者权限较大，删除数据会影响稳定性，查看具体功能，可以单独部署在u_user 表中修改)

### 运行效果（view）
1.在浏览器输入 http://localhost:3000/ 如下图

![image](https://gitee.com/wuxw7/MicroCommunity/raw/master/docs/img/login.png)

    用户名为 wuxw 密码为 admin  如下图

    点击登录，进入如下图：
    
![image](https://gitee.com/wuxw7/MicroCommunity/raw/master/docs/img/index.png)

![image](https://gitee.com/wuxw7/MicroCommunity/raw/master/docs/img/owner.png)


### 加入我们（join）

加入HC小区交流群随时了解项目进度，和java110开发者零距离沟通 qq群号 827669685(已满)，799748606（群二），邮箱：928255095@qq.com

![image](https://gitee.com/wuxw7/MicroCommunity/raw/master/docs/img/qq.png)



