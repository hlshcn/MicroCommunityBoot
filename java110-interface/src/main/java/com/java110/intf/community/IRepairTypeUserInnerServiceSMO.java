package com.java110.intf.community;

import com.java110.dto.repair.RepairTypeUserDto;

import java.util.List;

/**
 * @ClassName IRepairTypeUserInnerServiceSMO
 * @Description 报修设置接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/

public interface IRepairTypeUserInnerServiceSMO {

    /**
     * <p>查询小区楼信息</p>
     *
     * @param repairTypeUserDto 数据对象分享
     * @return RepairTypeUserDto 对象数据
     */
    List<RepairTypeUserDto> queryRepairTypeUsers(RepairTypeUserDto repairTypeUserDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param repairTypeUserDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    int queryRepairTypeUsersCount(RepairTypeUserDto repairTypeUserDto);
}
