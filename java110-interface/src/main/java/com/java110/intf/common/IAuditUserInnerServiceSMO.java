package com.java110.intf.common;

import com.java110.dto.auditUser.AuditUserDto;

import java.util.List;

/**
 * @ClassName IAuditUserInnerServiceSMO
 * @Description 审核人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/

public interface IAuditUserInnerServiceSMO {

    /**
     * <p>查询小区楼信息</p>
     *
     * @param auditUserDto 数据对象分享
     * @return AuditUserDto 对象数据
     */
    List<AuditUserDto> queryAuditUsers(AuditUserDto auditUserDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param auditUserDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    int queryAuditUsersCount(AuditUserDto auditUserDto);
}
